const cli = require('../../lib/index.js');

const args = cli.argParser();
args.callerFile = __filename;
args.command('command', 'Command', 'subsubcommand.js');

module.exports = args;
