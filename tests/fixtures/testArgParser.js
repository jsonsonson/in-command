const cli = require('../../lib/index.js');
const otherCLI = cli.argParser();

if (process.env.DISABLE_HELP) {
  otherCLI.help = () => {};
  cli.help = () => {};
  otherCLI.version = () => {};
  cli.version = () => {};
}

function testArgParser(interactive) {
  const argParser = cli.argParser();

  if (process.env.DISABLE_HELP) {
    argParser.version = () => {};
  }

  argParser
    .version('1.0.0')
    .command('c1', 'Command1', () => {})
    // .command('c2', 'Command2')
    .command('c3', 'Command3', 'c3.js')
    .command('c4', '', otherCLI)
    // .command('c5 [param1] [param2] <param3...>', 'Command1', () => {})
    .description('Description')
    // .defaultCommand('c1')
    .option('o1', 'Option1')
    .option('o2', 'Option2', {
      default: 0,
      modifier: (value) => { return value + 1; },
    })
    .option('o3', 'Option3', {
      parameters: ['one', 'two'],
      paramSeparator: '.',
      short: 'o3',
    })
    .option('o4', 'Option4', {
      default: 'x',
    })
    // .parameter('p1')
    // .parameter('p2', true)
    // .parameter('p4', false)
    // .parameter('p3', true, true)
    // .parameter('p5', false, false)
    // .parameter('p6', true, false)
    // .parameter('p7', false, true)
    .parameterSeparator('-')
    .onResult(() => {})
    // .on('exec', () => {})
    // .on('help', () => {})
    .on('version', () => {})
    .on('o1', () => {})
    .on('p1', () => {})
    // .usage('Usage')
    .description('description')
    .beforeHelpLog('ello')
    .afterHelpLog('goodbye')
    .versionOptionDescription('Version')
    .helpOptionDescription('Help')
    .helpPadding('-- ')
    .disableMissingRequiredMessage();

  if (interactive) {
    argParser.enableInteractivity('prompt: ', 'exit', 'Exit');
  }

  return argParser;
}

module.exports = testArgParser;
