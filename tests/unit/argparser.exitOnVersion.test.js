const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set exit on version', () => {
      argParser.exitOnVersion(true);
      expect(argParser.exitVersion).to.equal(true);

      argParser.exitOnVersion(false);
      expect(argParser.exitVersion).to.equal(false);
    });
  });
});
