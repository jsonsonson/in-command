const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should enable commands in the help output', () => {
      argParser.enableHelpCommands();
      expect(argParser.commandsHelpEnabled).to.equal(true);
    });
  });
});
