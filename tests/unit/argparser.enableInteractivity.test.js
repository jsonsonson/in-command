const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    context('with no parameters', () => {
      it('should enable interactivity', () => {
        argParser.enableInteractivity();
        expect(argParser.interactive).to.equal(true);
      });
    });

    context('with the prompt parameter', () => {
      it('should enable interactivity', () => {
        const prompt = '.';
        argParser.enableInteractivity(prompt);

        expect(argParser.interactive).to.equal(true);
        expect(argParser.prompt.includes(prompt)).to.equal(true);
      });
    });

    context('with the prompt parameter and quit command name parameter', () => {
      it('should enable interactivity', () => {
        const prompt = '.';
        const cmd = 'stop';
        argParser.enableInteractivity(prompt, cmd);

        expect(argParser.interactive).to.equal(true);
        expect(argParser.prompt.includes(prompt)).to.equal(true);
        expect(argParser.quitCommand.includes(cmd)).to.equal(true);
      });
    });

    context('with the prompt parameter, quit command name parameter, and quit description parameter', () => {
      it('should enable interactivity', () => {
        const prompt = '.';
        const cmd = 'stop';
        const desc = 'description';
        argParser.enableInteractivity(prompt, cmd, desc);

        expect(argParser.interactive).to.equal(true);
        expect(argParser.prompt.includes(prompt)).to.equal(true);
        expect(argParser.quitCommand.includes(cmd)).to.equal(true);
        expect(argParser.quitCommandDesc.includes(desc)).to.equal(true);
      });
    });
  });
});
