const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    context('with only the parameter name', () => {
      it('should add the parameter', () => {
        const paramName = 'param1';
        argParser.parameter(paramName);

        const p = argParser.parameters.find(par => par._n === paramName);
        p.build();

        expect(p).to.not.be.undefined;
        expect(p.required).to.be.undefined;
        expect(p.repeated).to.be.undefined;
      });
    });

    context('with required', () => {
      it('should add the parameter', () => {
        const paramName = 'param2';
        argParser.parameter(paramName, true);

        const p = argParser.parameters.find(par => par._n === paramName);
        p.build();

        expect(p).to.not.be.undefined;
        expect(p.required).to.equal(true);
        expect(p.repeated).to.be.undefined;
      });
    });

    context('with both required and repeated', () => {
      it('should add the parameter', () => {
        const paramName = 'param3';
        argParser.parameter(paramName, true, true);

        const p = argParser.parameters.find(par => par._n === paramName);
        p.build();

        expect(p).to.not.be.undefined;
        expect(p.required).to.equal(true);
        expect(p.repeated).to.equal(true);
      });
    });
  });
});
