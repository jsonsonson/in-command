const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should enable the quit interactivity command', () => {
      argParser
        .disableQuitInteractivityCommand()
        .enableQuitInteractivityCommand();

      expect(argParser.quitEnabled).to.equal(true);
    });
  });
});
