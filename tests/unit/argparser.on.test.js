const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should add the event', () => {
      const evt = 'exec';
      argParser.on(evt, () => {});

      expect(argParser.eventEmitter.listenerCount(evt)).to.equal(1);
    });
  });
});
