const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    context('with only the command name', () => {
      it('should set the quit command', () => {
        const cmd = 'abc';
        argParser.setQuitInteractivityCommand(cmd);

        expect(argParser.quitCommand.includes(cmd)).to.equal(true);
      });
    });

    context('with both the command name and description', () => {
      it('should set the quit command', () => {
        const cmd = 'abc';
        const desc = 'desc';
        argParser.setQuitInteractivityCommand(cmd, desc);

        expect(argParser.quitCommand.includes(cmd)).to.equal(true);
        expect(argParser.quitCommandDesc.includes(desc)).to.equal(true);
      });
    });
  });
});
