const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/index.js');

chai.use(chaiAsPromised);

const expect = chai.expect;

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set the version option name', () => {
      const name = 'revision';
      const argParser = cli
        .versionOption(name);

      expect(argParser.versionOpt.includes(name)).to.equal(true);
    });

    it('should set the version option short', () => {
      const name = 'revision';
      const short = 'x';
      const argParser = cli
        .versionOption(name, short);

      expect(argParser.versionOpt.includes(name)).to.equal(true);
      expect(argParser.versionOptShort.includes(short)).to.equal(true);
    });
  });
});
