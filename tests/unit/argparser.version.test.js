const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set the version', () => {
      const vers = '1.0.0';
      argParser.version(vers);

      expect(argParser.vers.includes(vers)).to.equal(true);
    });
  });
});
