const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const cli = require('../../lib/ArgParser.js');

chai.use(chaiAsPromised);

const expect = chai.expect;
const argParser = cli();

describe('ArgParser', () => {
  describe(`.${__filename.split('/').pop().split('.')[1]}`, () => {
    it('should set the help option name', () => {
      const name = 'abc';
      argParser.helpOption(name);
      const helpOption = argParser.options.find(option => option._n === name);

      expect(helpOption._n.includes(name)).to.equal(true);
    });

    it('should set the help option name and short', () => {
      const name = 'abc';
      const short = 'c';
      argParser.helpOption(name, short);
      const helpOption = argParser.options.find(option => option._n === name);

      expect(helpOption._n.includes(name)).to.equal(true);
      expect(helpOption.config.short.includes(short)).to.equal(true);
    });
  });
});
