const cli = require('in-command');
const store = require(`${__dirname}/Store.js`);

const args = cli
  .on('exec', (options, parameters, command) => {
    if (parameters.item !== undefined) {
      store.setPrice(parameters.item, parameters.price || 0);
    }
  });

module.exports = args;
