// const uglifyjs = require('uglify-js');
const fs = require('fs');

module.exports = () => {
  // let code = {};

  const stagedFiles = fs.readdirSync('stage');

  if (!fs.existsSync('dist')) {
    fs.mkdirSync('dist');
  }

  stagedFiles.forEach(file => {
    const uglyFile = `dist/${file}`;
    const filePath = `stage/${file}`;
    // code[uglyFile] = fs.readFileSync(filePath).toString();
    // const result = uglifyjs.minify(code);

    if (fs.existsSync(uglyFile)) {
      fs.unlinkSync(uglyFile);
    }

    // fs.writeFileSync(uglyFile, result.code);
    fs.writeFileSync(uglyFile, fs.readFileSync(filePath).toString());

    // code = {};
  });

  console.log('Uglify finished');
};
