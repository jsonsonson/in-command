# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.5] - 2020-02-11
### Changed
- Minor package require fixes to allow React to behave when in-command is used

## [1.2.4] - 2020-02-10
### Changed
- Fix binary files attempting to only execute as exe
- minor bug fixes

### Added
- Regex command support
- parseOnExit function

## [1.1.4] - 2019-01-23
### Changed
- Fix for interactive crashing on empty args

## [1.1.3] - 2019-01-23
### Changed
- Fix for interactive commands failing after repeating the same command

## [1.1.2] - 2019-01-22
### Changed
- Fixed recent changes not appearing in NPM

## [1.1.1] - 2019-01-22
### Changed
- Fix for interactive options being split when surrounded by quotes

## [1.1.0] - 2019-01-22
### Changed
- README fix
- Execute before event after options/params validation

### Added
- Support for executing bat and cmd files with commands

## [1.0.0] - 2018-09-16

### Added
- 1.0.0 release (moved from wily-cli)

[Unreleased]: https://gitlab.com/jsonsonson/in-command/compare/v1.2.5...HEAD
[1.2.5]: https://gitlab.com/jsonsonson/in-command/compare/v1.2.4...v1.2.5
[1.2.4]: https://gitlab.com/jsonsonson/in-command/compare/v1.1.4...v1.2.4
[1.1.4]: https://gitlab.com/jsonsonson/in-command/compare/v1.1.3...v1.1.4
[1.1.3]: https://gitlab.com/jsonsonson/in-command/compare/v1.1.2...v1.1.3
[1.1.2]: https://gitlab.com/jsonsonson/in-command/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/jsonsonson/in-command/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/jsonsonson/in-command/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/jsonsonson/in-command/compare/v0.0.0...v1.0.0
