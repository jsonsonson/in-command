if (!process.env.GITLAB_CI) {
  const prompt = require('prompt');
  const uglify = require('./uglify.js');

  const todos = [];
  prompt.message = 'y/n';

  console.log('Prepublish Checklist...');
  prompt.start();
  prompt.get([
    '\x1b[34mnpm run git\x1b[0m executed',
    'README updated with new features and uses',
  ], (err, result) => {
    if (!err) {
      const resultKeys = Object.keys(result);

      resultKeys.forEach((key, index) => {
        const answer = result[key].toLowerCase();

        if (answer.charAt(0) !== 'y') {
          todos.push(key);
        }
      });

      if (todos.length > 0) {
        console.log();
        console.log('Update everything before publishing to npm');
        console.log('Todos:\n')

        todos.forEach((todo) => {
          console.log(`- ${todo}`);
        });

        process.exit(1);
      }

      uglify();
    } else {
      console.log();
      process.exit(1);
    }
  });
}
