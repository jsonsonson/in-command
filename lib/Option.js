const values = require('./values.js');
const { formatStringToConsole } = require('./helpers');

class Option {

  constructor (name, description, argParser, config) {
    this._n = name;
    this.description = description;
    this.argParser = argParser;
    this.config = config;
    this.built = false;
  }

  build() {
    if (!this.built) {
      this.built = true;

      const adjustedName = `${this._n}`.replace(values.invalidCLIChars, '').trim();
      let paramIndex = adjustedName.indexOf('<');
      const tempParamIndex = adjustedName.indexOf('[');

      if (tempParamIndex > 0 && tempParamIndex < paramIndex) {
        paramIndex = tempParamIndex;
      }

      if (paramIndex > 0) {
        this.name = adjustedName.substring(0, paramIndex).trim();
        let params = adjustedName.substring(paramIndex);

        if (params.length > 0) {
          params = params.match(/[<[][^>\]]+[>\[]/g) || [];

          params.forEach(param => {
            // const isRequired = param.charAt(0) === '<'; // TODO required option parameter check?
            const p = param.substring(1, param.length - 1);

            if (p.length > 0) {
              if (this.config === undefined) {
                this.config = {
                  parameters: [],
                };
              }

              if (this.config.parameters === undefined) {
                this.config.parameters = [];
              }

              if (!this.config.parameters.includes(p)) {
                this.config.parameters.push(p);
              }
            }
          });
        }
      } else {
        this.name = adjustedName;
      }

      let short;
      if (!this.argParser.cli.options[this.name]) {
        short = this.config && this.config.short !== undefined ? `${this.config.short}` : this.name.charAt(0);
      }

      if (short !== undefined) {
        short = short.replace(values.invalidCLIChars, '').trim();

        const shortLower = short.toLowerCase() === short;
        if (this.argParser.cli.options._shorts[short]) {
          if (shortLower) {
            short = short.toUpperCase();
          } else {
            short = short.toLowerCase();
          }

          if (this.argParser.cli.options._shorts[short]) {
            short = undefined;
          }
        }
      }

      if (short !== undefined) {
        this.short = short;
        this.argParser.cli.options._shorts[short] = this.name;
      }

      if (this.config !== undefined) {
        if (this.config.default !== undefined) {
          this.default = this.config.default;
        }

        if (this.config.modifier !== undefined && this.config.modifier.constructor === Function) {
          this.modifier = this.config.modifier;
        }

        if (this.config.parameters !== undefined && this.config.parameters.constructor === Array) {
          this.parameters = this.config.parameters;
        }

        if (this.config.paramSeparator !== undefined &&
          (this.config.paramSeparator.constructor === String || this.config.paramSeparator.constructor === Number)) {
          this.paramSeparator = this.config.paramSeparator;
        }

        if (this.config.required) {
          this.required = this.config.required;
        }

        if (this.config.passThrough) {
          this.isGlobal = this.config.passThrough;
        }
      }

      this.argParser.cli.options[this.name] = this;
    }
  }

  toString(switchChar) {
    let optionStr;

    if (this.short !== undefined && this.short.length > 0) {
      optionStr = `${switchChar || '-'}${this.short}, ${switchChar || '--'}${this.name}`;
    } else {
      optionStr = `${switchChar || '--'}${this.name}`;
    }

    if (this.parameters) {
      let params = [];

      this.parameters.forEach((param) => {
        params.push(`<${param}>`);
      });

      params = params.join(this.paramSeparator || ' ');
      optionStr = `${optionStr} ${params}`;
    }

    this.optionStr = optionStr;

    return optionStr;
  }

  help(longestOption, switchChar) {
    let helpStr = this.optionStr || this.toString(switchChar);

    const optionLength = helpStr.length;
    const lengthDiff = longestOption - optionLength;
    const padding = Array(lengthDiff).join(' ');
    const before = `${this.argParser.helpPaddingStr}  ${values.colors.yellow}${helpStr}${padding}  `;
    const after = formatStringToConsole(`${values.colors.white}${this.description}${values.colors.clear}`, before);
    helpStr = `${before}${after}`;

    if (this.required) {
      helpStr = `${helpStr}\n\t  ${Array(optionLength).join(' ')}${padding}     ${values.colors.grey}required${values.colors.clear}`;
    }

    if (this.default !== undefined) {
      let defaultStr = this.default;
      if (this.default.constructor === Object) {
        const defaults = [];
        Object.keys(this.default).forEach((key) => {
          defaults.push(`${key}: ${this.default[key]}`);
        });

        defaultStr = defaults.join(', ');
      } else if (this.default.constructor === Array) {
        defaultStr = `[${this.default.join(', ')}]`;
      }

      const defaultOut = `${values.colors.magenta}default ${values.colors.cyan}${defaultStr}${values.colors.clear}`;
      helpStr = `${helpStr}\n\t  ${Array(optionLength).join(' ')}${padding}     ${defaultOut}`;
    }

    return helpStr;
  }
}

module.exports = Option;
