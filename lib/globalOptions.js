let globalOptions = {};

function add(opt, value) {
  globalOptions[opt] = value;
}

function clear() {
  globalOptions = {};
}

function set(newGlobalOptions) {
  globalOptions = newGlobalOptions;
}

function get() {
  return globalOptions;
}

module.exports = {
  add,
  clear,
  get,
  set,
};
