const { spawnSync } = require('child_process');
const version = require('./package.json').version;
const tag = `v${version}`;
const cli = require('./lib/index.js');

cli
  .parameter('message', true, true)
  .on('exec', (options, parameters) => {
    if (parameters.message) {
      let message = parameters.message;

      if (message.constructor === Array) {
        message = message.join(' ');
      }

      spawnSync('git', ['tag', '-d', tag]);
      spawnSync('git', ['tag', '-a', tag, '-m', message]);
      spawnSync('git', ['push', 'origin', `:${tag}`]);
      spawnSync('git', ['push', 'origin', tag]);
    } else {
      console.log('Please provide a message');
    }
  });
